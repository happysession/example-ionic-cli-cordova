// adaptation of @see https://gist.github.com/ajinabraham/bf2945741346c2fcdcd1b2e3df8baab0
const request = require('request');
const {createReadStream} = require('fs');
const stream = createReadStream('./platforms/android/app/build/outputs/apk/debug/app-debug.apk');

(async () => {
    const formData = {
        file: stream,
    };    
    request.post({headers: {Authorization: 'key'}, url: 'http://0.0.0.0:8000/api/v1/upload', formData: formData
    }, (err, httpResponse, body) => {
        if (err) {
            return console.error('upload failed:', err);
        }
        console.log('Upload successful!  Server responded with:', body);
        const scanData = JSON.parse(body)
        let rescan = '0';
        console.log(scanData.hash)
        request.post({headers: {Authorization: 'key'}, url: 'http://localhost:8000/api/v1/scan',  formData: {
                "hash": scanData.hash,
                "scan_type": scanData.scan_type,
                "file_name": scanData.file_name,
                "re_scan": rescan
            }
        }, (err, httpResponse, body) => {
            if (err) {
                return console.error('scan failed:', err);
            }
            console.log('Scan successful!  Server responded with:', body);
            const scanobj = JSON.parse(body)
            scan_hash = scanobj.hash;
            scan_type = scanobj.scan_type;
            file_name = scanobj.file_name;
        }, err => console.error('scan failed:', err));
    }, err => console.error('upload failed:', err));
})();



