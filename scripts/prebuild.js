const fs = require('fs');
const path = require('path');

const ejs = require('ejs');

const environmentFilesDirectory = path.join(__dirname, '../src/environments');


let args = process.argv.slice(2);

let env = 'dev';  //SETTING DEFAULT ENV AS DEV
if (args[0] == 'prod') {
  process.env.PRODUCTION = true;
  env = 'prod';
  console.log('\x1b[31m%s\x1b[0m', '/!\\ Production env.'); 
} else {
  process.env.PRODUCTION = false;
  console.log('\x1b[36m%s\x1b[0m', '/!\\ Dev env.'); 
} 
const targetEnvironmentTemplateFileName = `environment.env.ts.template`;
const targetEnvironmentFileName = `environment.${env}.ts`;

// Define default values in case there are no defined ones,
// but you should define only non-crucial values here,
// because build should fail if you don't provide the correct values
// for your production environment
const defaultEnvValues = {
  PRODUCTION : false,
  CI_SECRET_VARIABLE: 'not declared yet!',
};

// Load template file
const environmentTemplate = fs.readFileSync(
  path.join(environmentFilesDirectory, targetEnvironmentTemplateFileName),
  {encoding: 'utf-8'}
);

// Generate output data
const output = ejs.render(environmentTemplate, Object.assign({}, defaultEnvValues, process.env));
// Write environment file
fs.writeFileSync(path.join(environmentFilesDirectory, targetEnvironmentFileName), output);

process.exit(0);